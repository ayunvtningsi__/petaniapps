/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Kritik;

import entity.KritikSaran;
import entity.Postingan;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import org.hibernate.Query;
import org.hibernate.Session;
import util.NewHibernateUtil;

/**
 * FXML Controller class
 *
 * @author Administator
 */
public class KritikAdminController implements Initializable {

    @FXML
    private AnchorPane pane;
    @FXML
    private TextField tanggalF;
    @FXML
    private TextArea isiF;
    @FXML
    private TableView<KritikSaran> TabelKritikSaran;
    @FXML
    private TableColumn idT;
    @FXML
    private TableColumn tanggalT;
    @FXML
    private TableColumn isiT;
    @FXML
    private Button deleteButton;
    @FXML
    private Button menu;
    
    ObservableList<KritikSaran> data = null;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
         data = FXCollections.observableArrayList();
        Model mm = new Model();

        for (KritikSaran m : mm.findAll()) {
            data.add(m);
    }    
        idT.setCellValueFactory(new PropertyValueFactory<>("id"));
        tanggalT.setCellValueFactory(new PropertyValueFactory<>("tanggal"));
        isiT.setCellValueFactory(new PropertyValueFactory<>("isi"));
        TabelKritikSaran.setItems(data);
    }    

    @FXML
    private void fromTableToTextField() {
        TabelKritikSaran.setOnMouseClicked((MouseEvent event)-> {
            KritikSaran p = TabelKritikSaran.getItems().get(TabelKritikSaran.getSelectionModel().getSelectedIndex());
            
            tanggalF.setText(p.getTanggal());
            isiF.setText(p.getIsi());
        });
    }

    @FXML
    private void deleteAction(ActionEvent event) throws IOException {
          if (isiF.getText().equals("")) {
            Alert alert = new Alert(Alert.AlertType.NONE, "Semua field harus di isi", ButtonType.OK);
            alert.setTitle("Gagal");
            alert.showAndWait();
        } else {
             
            executeHQLQueryDelete();
            Alert alert = new Alert(Alert.AlertType.NONE, "Layanan telah dihapus", ButtonType.OK);
            alert.setTitle("Berhasil");
             alert.showAndWait();
        }
        
        Stage stage;
                Parent root;
                //get reference to the button's stage         
                stage = (Stage) deleteButton.getScene().getWindow();
                //load up OTHER FXML document
                root = FXMLLoader.load(getClass().getResource("KritikAdmin.fxml"));
                
                Scene scene = new Scene(root);
                stage.setScene(scene);
                stage.show();
    }

    @FXML
    private void menuAction(ActionEvent event) throws IOException {
          Stage stage;
                Parent root;
                //get reference to the button's stage         
                stage = (Stage) menu.getScene().getWindow();
                //load up OTHER FXML document
                root = FXMLLoader.load(getClass().getResource("/petaniapps/HomepageAdmin.fxml"));
                Scene scene = new Scene(root);
                stage.setScene(scene);
                stage.show();
    }

    private void executeHQLQueryDelete() {
         try {
            Session session = NewHibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            Query q = session.createSQLQuery ("DELETE FROM kritik_saran WHERE isi = :isi");
            q.setParameter("isi", isiF.getText());
            q.executeUpdate();
            session.getTransaction().commit();
            session.close();
            
            //pindah();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    }
    


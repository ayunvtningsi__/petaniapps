/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Postingan;


import entity.Postingan;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.CopyOption;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javax.imageio.ImageIO;
import org.hibernate.Query;
import org.hibernate.Session;
import util.NewHibernateUtil;

/**
 * FXML Controller class
 *
 * @author Administator
 */
public class PostinganAdminController implements Initializable {

    @FXML
    private TableView<Postingan> TabelPostingan;
    @FXML
    private TableColumn<?, ?> namaUserT1;
    @FXML
    private TableColumn<?, ?> tanggalT1;
    @FXML
    private TableColumn<?, ?> isiT1;
    @FXML
    private TableColumn<?, ?> fotoT1;
    @FXML
    private TextField namaUserF;
    @FXML
    private TextField tanggalF;
    @FXML
    private TextArea isiF;
    @FXML
    private Button fotoF;
    private Button addButton;
    @FXML
    private Button deleteBtn;
    
    
    ObservableList<Postingan> data = null;
     
    private FileChooser fileChooser;
    private File file;
    private String gambar;
    private Path copy,files;
    private implementsDao implement;
    @FXML
    private Button menu;
    @FXML
    private ImageView lihatGambar;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        implement = new PostinganController();
        fileChooser = new FileChooser();
        fileChooser.getExtensionFilters().addAll(
        new FileChooser.ExtensionFilter("All Files", "*.png","*.jpg","*jpeg","*.bmp", "*.mp4")
        );
          data = FXCollections.observableArrayList();
        Model mm = new Model();
        
         for (Postingan m : mm.findAll()) {
            data.add(m);
        }
         
        namaUserT1.setCellValueFactory(new PropertyValueFactory<>("namaUser"));
        tanggalT1.setCellValueFactory(new PropertyValueFactory<>("tanggal"));
        isiT1.setCellValueFactory(new PropertyValueFactory<>("isi"));
        fotoT1.setCellValueFactory(new PropertyValueFactory<>("foto"));
        //tv.getColumns().clear();
        TabelPostingan.setItems(data);
        
    }    

    @FXML
    private void fromTableToTextField() {
            TabelPostingan.setOnMouseClicked((MouseEvent event)-> {
            Postingan p = TabelPostingan.getItems().get(TabelPostingan.getSelectionModel().getSelectedIndex());
            namaUserF.setText(p.getNamaUser());
            tanggalF.setText(p.getTanggal());
            isiF.setText(p.getIsi());
        });
    }

//    @FXML
//    private void uploadAction(ActionEvent event) {
//        file =  fileChooser.showOpenDialog(null);
//        if(file != null){
//        try {
//            BufferedImage bufferedImage = ImageIO.read(file);
//            Image image = SwingFXUtils.toFXImage(bufferedImage, null);
//            lihatGambar.setFitWidth(180);
//            lihatGambar.setFitHeight(110);
//            lihatGambar.setPreserveRatio(true);
//            lihatGambar.setImage(image);
//            gambar = file.getName();
//            files = Paths.get(file.toURI());
//        } catch (IOException ex) {
//            Logger.getLogger(PostinganController.class.getName()).log(Level.SEVERE, null, ex);
//        }
//    }
//    }
//    private void addAction(ActionEvent event) throws IOException {
//    if (namaUserF.getText().equals("")) {
//            Alert alert = new Alert(Alert.AlertType.NONE, "Semua field harus di isi", ButtonType.OK);
//            alert.setTitle("Gagal");
//            alert.showAndWait();
//        } else {
//
//                Postingan upload = new Postingan();
//                upload.setNamaUser(namaUserF.getText());
//                upload.setTanggal(tanggalF.getText());
//                upload.setIsi(isiF.getText());
//                upload.setFoto(gambar);
//                implement.createUpload(upload);
//        
//         if (gambar != null) {
//        try {
//            File dir = new File(System.getProperty("user.dir"));
//            copy = Paths.get(dir+"/src/images/"+gambar);
//            CopyOption[] options = new CopyOption[]{
//                    StandardCopyOption.REPLACE_EXISTING,
//                    StandardCopyOption.COPY_ATTRIBUTES
//            };
//            Files.copy(files, copy, options);
//        } catch (IOException ex) {
//            Logger.getLogger(PostinganController.class.getName()).log(Level.SEVERE, null, ex);
//       }
//    }              
//  }
//Stage stage;
//                Parent root;
//                //get reference to the button's stage         
//                stage = (Stage) addButton.getScene().getWindow();
//                //load up OTHER FXML document
//                root = FXMLLoader.load(getClass().getResource("Postingan.fxml"));
//                
//                Scene scene = new Scene(root);
//                stage.setScene(scene);
//                stage.show();    
//}
//
//    private void updateAction(ActionEvent event) throws IOException {
//        if (namaUserF.getText().equals("")) {
//            Alert alert = new Alert(Alert.AlertType.NONE, "Semua field harus di isi", ButtonType.OK);
//            alert.setTitle("Gagal");
//            alert.showAndWait();
//        } else {
//             
//           executeHQLQueryUpdate();
//           Stage stage;
//                Parent root;
//                //get reference to the button's stage         
//                stage = (Stage) addButton.getScene().getWindow();
//                //load up OTHER FXML document
//                root = FXMLLoader.load(getClass().getResource("Postingan.fxml"));
//                
//                Scene scene = new Scene(root);
//                stage.setScene(scene);
//                stage.show();
//        }
//    }

    @FXML
    private void deleteAction(ActionEvent event) throws IOException {
        if (namaUserF.getText().equals("")) {
            Alert alert = new Alert(Alert.AlertType.NONE, "Semua field harus di isi", ButtonType.OK);
            alert.setTitle("Gagal");
            alert.showAndWait();
        } else {
             
            executeHQLQueryDelete();
            Alert alert = new Alert(Alert.AlertType.NONE, "Yakin untuk dihapus?", ButtonType.OK);
            alert.setTitle("Berhasil");
             alert.showAndWait();
        }
        Stage stage;
                Parent root;
                //get reference to the button's stage         
                stage = (Stage) addButton.getScene().getWindow();
                //load up OTHER FXML document
                root = FXMLLoader.load(getClass().getResource("Postingan.fxml"));
                
                Scene scene = new Scene(root);
                stage.setScene(scene);
                stage.show();
    }

//    private void executeHQLQueryUpdate() {
//        try {
//            Session session = NewHibernateUtil.getSessionFactory().openSession();
//            session.beginTransaction();
//            Query q = session.createSQLQuery ("UPDATE postingan SET  tanggal =:tanggal, isi =:isi Where nama_user = :nama_user");
//         
//            q.setParameter("nama_user", namaUserF.getText());
//            q.setParameter("tanggal", tanggalF.getText());
//            q.setParameter("isi", isiF.getText());
//            q.executeUpdate();
//            session.getTransaction().commit();
//            session.close();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        
//    }

    private void executeHQLQueryDelete() {
        try {
            Session session = NewHibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            Query q = session.createSQLQuery ("DELETE FROM postingan WHERE nama_user = :nama_user");
         
            q.setParameter("nama_user", namaUserF.getText());
            q.executeUpdate();
            session.getTransaction().commit();
            session.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
//  public void createUpload(Postingan upload) {
//        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
//        alert.setTitle("Apakah anda akan menyimpan data");
//        alert.setHeaderText(null);
//        alert.setContentText("Tekan OK untuk menyimpan data, Cencel untuk batal.");
//        Optional result = alert.showAndWait();
//        
//        if (result.get() == ButtonType.OK){
//            Session session = NewHibernateUtil.getSessionFactory().openSession();
//            session.beginTransaction();
//             //Query q = session.createSQLQuery("INSERT INTO upload (foto) VALUES (:foto)");
//              Query q = session.createSQLQuery("INSERT INTO postingan (nama_user,tanggal,isi,foto) VALUES (?,?,?,?)");
//            
//            q.setString(0, upload.getNamaUser());
//            q.setString(1, upload.getTanggal());
//            q.setString(2, upload.getIsi());
//            q.setString(3, upload.getFoto());
//            q.executeUpdate();
//            session.getTransaction().commit();
//            session.close();
//        }
//    }

    @FXML
    private void menuAction(ActionEvent event) throws IOException {
        Stage stage;
                Parent root;
                //get reference to the button's stage         
                stage = (Stage) addButton.getScene().getWindow();
                //load up OTHER FXML document
                root = FXMLLoader.load(getClass().getResource("/petaniapps/Homepage.fxml"));
                
                Scene scene = new Scene(root);
                stage.setScene(scene);
                stage.show();
    }

    @FXML
    private void uploadAction(ActionEvent event) {
    }
}

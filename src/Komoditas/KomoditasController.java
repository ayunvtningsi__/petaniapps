/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Komoditas;


import entity.Komoditas;
import java.io.File;
import java.net.URL;
import java.nio.file.Path;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.FileChooser;


import Komoditas.Model;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.nio.file.CopyOption;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.embed.swing.SwingFXUtils;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import javax.imageio.ImageIO;
import org.hibernate.Query;
import org.hibernate.Session;
import util.NewHibernateUtil;

/**
 * FXML Controller class
 *
 * @author Administator
 */
public class KomoditasController implements Initializable, implementsDao {

    @FXML
    private AnchorPane pane;
    @FXML
    private TextField namaF;
    @FXML
    private TextField jenisF;
    @FXML
    private TextArea deskripsiF;
    @FXML
    private Button fotoF;
    @FXML
    private TableView<Komoditas> TabelKomoditas;
    @FXML
    private TableColumn namaT;
    @FXML
    private TableColumn jenisT;
    @FXML
    private TableColumn deskripsiT;
    @FXML
    private TableColumn fotoT;
    @FXML
    private Button addButton;
    @FXML
    private Button deleteBtn;
    ObservableList<Komoditas> data = null;
    
  
    private FileChooser fileChooser;
    private File file;
    private String gambar;
    private Path copy,files;
    private implementsDao implement;
    @FXML
    private Button updateBtn;
    @FXML
    private Button menu;
    @FXML
    private ImageView lihatGambar;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
          implement = new KomoditasController();
        fileChooser = new FileChooser();
        fileChooser.getExtensionFilters().addAll(
        new FileChooser.ExtensionFilter("All Files", "*.png","*.jpg","*jpeg","*.bmp", "*.mp4")
    ); 
        data = FXCollections.observableArrayList();
        Model mm = new Model();
        
         for (Komoditas m : mm.findAll()) {
            data.add(m);
        }
        
        namaT.setCellValueFactory(new PropertyValueFactory<>("nama"));
        jenisT.setCellValueFactory(new PropertyValueFactory<>("jenis"));
        deskripsiT.setCellValueFactory(new PropertyValueFactory<>("deskripsi"));
        fotoT.setCellValueFactory(new PropertyValueFactory<>("foto"));
       
        TabelKomoditas.setItems(data);
    }    

    @FXML
    private void uploadAction(ActionEvent event) {
             file =  fileChooser.showOpenDialog(null);
    if(file != null){
        try {
            BufferedImage bufferedImage = ImageIO.read(file);
            Image image = SwingFXUtils.toFXImage(bufferedImage, null);
            lihatGambar.setFitWidth(180);
            lihatGambar.setFitHeight(110);
            lihatGambar.setPreserveRatio(true);
            lihatGambar.setImage(image);
            gambar = file.getName();
            files = Paths.get(file.toURI());
        } catch (IOException ex) {
            Logger.getLogger(KomoditasController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    }

    @FXML
    private void fromTableToTextField() {
        TabelKomoditas.setOnMouseClicked((MouseEvent event) -> {
            Komoditas b = TabelKomoditas.getItems().get(TabelKomoditas.getSelectionModel().getSelectedIndex());
            namaF.setText(b.getNama());
            jenisF.setText(b.getJenis());
            deskripsiF.setText(b.getDeskripsi());
            
        });
        
    }

    @FXML
    private void addAction(ActionEvent event) throws IOException {
         if (namaF.getText().equals("")) {
            Alert alert = new Alert(Alert.AlertType.NONE, "Semua field harus di isi", ButtonType.OK);
            alert.setTitle("Gagal");
            alert.showAndWait();
        } else {    
                Komoditas upload = new Komoditas();
                upload.setNama(namaF.getText());
                upload.setJenis(jenisF.getText());
                upload.setDeskripsi(deskripsiF.getText());
                upload.setFoto(gambar);
                implement.createUpload(upload);
        
         if (gambar != null) {
        try {
            File dir = new File(System.getProperty("user.dir"));
            copy = Paths.get(dir+"/src/images/"+gambar);
            CopyOption[] options = new CopyOption[]{
                    StandardCopyOption.REPLACE_EXISTING,
                    StandardCopyOption.COPY_ATTRIBUTES
            };
            Files.copy(files, copy, options);
        } catch (IOException ex) {
            Logger.getLogger(KomoditasController.class.getName()).log(Level.SEVERE, null, ex);
        }
    } 
                
                
    }
           Stage stage;
                Parent root;
                //get reference to the button's stage         
                stage = (Stage) addButton.getScene().getWindow();
                //load up OTHER FXML document
                root = FXMLLoader.load(getClass().getResource("Komoditas.fxml"));
                
                Scene scene = new Scene(root);
                stage.setScene(scene);
                stage.show();
    }

    @FXML
    private void updateAction(ActionEvent event) throws IOException {
         if (namaF.getText().equals("")) {
            Alert alert = new Alert(Alert.AlertType.NONE, "Semua field harus di isi", ButtonType.OK);
            alert.setTitle("Gagal");
            alert.showAndWait();
        } else {
             
           executeHQLQueryUpdate();
           
            Stage stage;
                Parent root;
                //get reference to the button's stage         
                stage = (Stage) updateBtn.getScene().getWindow();
                //load up OTHER FXML document
                root = FXMLLoader.load(getClass().getResource("Komoditas.fxml"));
                
                Scene scene = new Scene(root);
                stage.setScene(scene);
                stage.show();
        }
    }

    @FXML
    private void deleteAction(ActionEvent event) throws IOException {
        
        if (namaF.getText().equals("")) {
            Alert alert = new Alert(Alert.AlertType.NONE, "Semua field harus di isi", ButtonType.OK);
            alert.setTitle("Gagal");
            alert.showAndWait();
        } else {
             
            executeHQLQueryDelete();
            Alert alert = new Alert(Alert.AlertType.NONE, "Layanan telah dihapus", ButtonType.OK);
            alert.setTitle("Berhasil");
             alert.showAndWait();
              Stage stage;
                Parent root;
                //get reference to the button's stage         
                stage = (Stage) deleteBtn.getScene().getWindow();
                //load up OTHER FXML document
                root = FXMLLoader.load(getClass().getResource("Komoditas.fxml"));
                
                Scene scene = new Scene(root);
                stage.setScene(scene);
                stage.show();
        }
    }

    private void executeHQLQueryDelete() {
        try {
            Session session = NewHibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            Query q = session.createSQLQuery ("DELETE FROM komoditas WHERE nama = :nama");
         
            q.setParameter("nama", namaF.getText());
            

            q.executeUpdate();
            
            session.getTransaction().commit();
            
            session.close();
            
            //pindah();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    

    @Override
    public void createUpload(Komoditas upload) {
          Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Apakah anda akan menyimpan data");
        alert.setHeaderText(null);
        alert.setContentText("Tekan OK untuk menyimpan data, Cencel untuk batal.");
        Optional result = alert.showAndWait();
        
        if (result.get() == ButtonType.OK){
            Session session = NewHibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
             //Query q = session.createSQLQuery("INSERT INTO upload (foto) VALUES (:foto)");
              Query q = session.createSQLQuery("INSERT INTO komoditas (nama,jenis,deskripsi,foto) VALUES (?,?,?,?)");
            
            q.setString(0, upload.getNama());
            q.setString(1, upload.getJenis());
            q.setString(2, upload.getDeskripsi());
            q.setString(3, upload.getFoto());
            q.executeUpdate();
            session.getTransaction().commit();
            session.close();
    
}
  }

    private void executeHQLQueryUpdate() {
        try {
            Session session = NewHibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            Query q = session.createSQLQuery ("UPDATE komoditas SET  nama = :nama ,jenis =:jenis, deskripsi =:deskripsi Where nama = :nama");
         
            q.setParameter("nama", namaF.getText());
            q.setParameter("jenis", jenisF.getText());
            
            q.setParameter("deskripsi", deskripsiF.getText());
           
           
            q.executeUpdate();
            
            session.getTransaction().commit();
            
            session.close();
            
            //pindah();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @FXML
    private void menuAction(ActionEvent event) throws IOException {
        Stage stage;
                Parent root;
                //get reference to the button's stage         
                stage = (Stage) addButton.getScene().getWindow();
                //load up OTHER FXML document
                root = FXMLLoader.load(getClass().getResource("/petaniapps/Homepage.fxml"));
                
                Scene scene = new Scene(root);
                stage.setScene(scene);
                stage.show();
    }   
}
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Profile;

import entity.Profile;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Administator
 */
public class ProfileController implements Initializable {

    @FXML
    private TableView<Profile> TabelProfile;
    ObservableList<Profile> data = null;
    @FXML
    private TableColumn namaT;
    @FXML
    private TableColumn usiaT;
    @FXML
    private TableColumn alamatT;
    @FXML
    private TableColumn notelpT;
    @FXML
    private TableColumn deskripsiT;
    @FXML
    private TableColumn fotoT;
    @FXML
    private Button menu;
    @FXML
    private Button addButton;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
         initializeTable();
    }    

    private void initializeTable() {
   data = FXCollections.observableArrayList();
        Model mm = new Model();

        for (Profile m : mm.findAll()) {
            data.add(m);
        }

        //nisnview.setCellValueFactory(new PropertyValueFactory<>("NISN"));
        namaT.setCellValueFactory(new PropertyValueFactory<>("nama"));
        usiaT.setCellValueFactory(new PropertyValueFactory<>("usia"));
        alamatT.setCellValueFactory(new PropertyValueFactory<>("alamat"));
        notelpT.setCellValueFactory(new PropertyValueFactory<>("noTelp"));
        deskripsiT.setCellValueFactory(new PropertyValueFactory<>("deskripsi"));
        fotoT.setCellValueFactory(new PropertyValueFactory<>("foto"));
        //tv.getColumns().clear();
        TabelProfile.setItems(data);
    }

    @FXML
    private void menuAction(ActionEvent event) throws IOException {
        Stage stage;
                Parent root;
                //get reference to the button's stage         
                stage = (Stage) menu.getScene().getWindow();
                //load up OTHER FXML document
                root = FXMLLoader.load(getClass().getResource("/petaniapps/Homepage.fxml"));
                
                Scene scene = new Scene(root);
                stage.setScene(scene);
                stage.show();
    }

    @FXML
    private void addAction(ActionEvent event) throws IOException {
         Stage stage;
                Parent root;
                //get reference to the button's stage         
                stage = (Stage) menu.getScene().getWindow();
                //load up OTHER FXML document
                root = FXMLLoader.load(getClass().getResource("Add.fxml"));
                
                Scene scene = new Scene(root);
                stage.setScene(scene);
                stage.show();
    }
    
}

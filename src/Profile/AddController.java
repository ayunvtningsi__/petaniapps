/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Profile;

import entity.Profile;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.CopyOption;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javax.imageio.ImageIO;
import org.hibernate.Query;
import org.hibernate.Session;
import util.NewHibernateUtil;

/**
 * FXML Controller class
 *
 * @author Administator
 */
public class AddController implements Initializable, implementsDao {

    @FXML
    private Button uploadBtn;
    @FXML
    private Button save;
    
    private FileChooser fileChooser;
    private File file;
    private String gambar;
    private Path copy,files;
    private implementsDao implement;
    ObservableList<Profile> data = null;
    @FXML
    private TextField namaF;
    @FXML
    private TextField usiaF;
    @FXML
    private TextField alamatF;
    @FXML
    private TextField notelpF;
    @FXML
    private TextArea deskripsiF;
    @FXML
    private ImageView lihatGambar;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
         implement = new AddController();
        fileChooser = new FileChooser();
        fileChooser.getExtensionFilters().addAll(
        new FileChooser.ExtensionFilter("All Files", "*.png","*.jpg","*jpeg","*.bmp", "*.mp4")
    ); 
        data = FXCollections.observableArrayList();
        Model mm = new Model();
        
         for (Profile m : mm.findAll()) {
            data.add(m);
        }
    }    

    @FXML
    private void uploadAction(ActionEvent event) {
         file =  fileChooser.showOpenDialog(null);
    if(file != null){
        try {
            BufferedImage bufferedImage = ImageIO.read(file);
            Image image = SwingFXUtils.toFXImage(bufferedImage, null);
            lihatGambar.setFitWidth(180);
            lihatGambar.setFitHeight(110);
            lihatGambar.setPreserveRatio(true);
            lihatGambar.setImage(image);
            gambar = file.getName();
            files = Paths.get(file.toURI());
        } catch (IOException ex) {
            Logger.getLogger(AddController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    }

    @FXML
    private void saveAction(ActionEvent event) throws IOException {
          if (namaF.getText().equals("")) {
            Alert alert = new Alert(Alert.AlertType.NONE, "Semua field harus di isi", ButtonType.OK);
            alert.setTitle("Gagal");
            alert.showAndWait();
        } else {    
                Profile upload = new Profile();
                upload.setNama(namaF.getText());
                upload.setUsia(usiaF.getText());
                upload.setAlamat(alamatF.getText());
                upload.setNoTelp(notelpF.getText());
                upload.setDeskripsi(deskripsiF.getText());
                upload.setFoto(gambar);
                implement.createUpload(upload);
        
         if (gambar != null) {
        try {
            File dir = new File(System.getProperty("user.dir"));
            copy = Paths.get(dir+"/src/images/"+gambar);
            CopyOption[] options = new CopyOption[]{
                    StandardCopyOption.REPLACE_EXISTING,
                    StandardCopyOption.COPY_ATTRIBUTES
            };
            Files.copy(files, copy, options);
        } catch (IOException ex) {
            Logger.getLogger(AddController.class.getName()).log(Level.SEVERE, null, ex);
        }
    } 
          }
          
          Stage stage;
                Parent root;
                //get reference to the button's stage         
                stage = (Stage) save.getScene().getWindow();
                //load up OTHER FXML document
                root = FXMLLoader.load(getClass().getResource("Profile.fxml"));
                
                Scene scene = new Scene(root);
                stage.setScene(scene);
                stage.show();
    }

    @Override
    public void createUpload(Profile upload) {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Apakah anda akan menyimpan data");
        alert.setHeaderText(null);
        alert.setContentText("Tekan OK untuk menyimpan data, Cencel untuk batal.");
        Optional result = alert.showAndWait();
        
        if (result.get() == ButtonType.OK){
            Session session = NewHibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
             //Query q = session.createSQLQuery("INSERT INTO upload (foto) VALUES (:foto)");
              Query q = session.createSQLQuery("INSERT INTO profile (nama,usia,alamat,no_telp,deskripsi,foto) VALUES (?,?,?,?,?,?)");
            
            q.setString(0, upload.getNama());
            q.setString(1, upload.getUsia());
            q.setString(2, upload.getAlamat());
            q.setString(3, upload.getNoTelp());
            q.setString(4, upload.getDeskripsi());
            q.setString(5, upload.getFoto());
            q.executeUpdate();
            session.getTransaction().commit();
            session.close();
        }
    }   
}
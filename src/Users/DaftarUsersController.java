/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Users;

import entity.KritikSaran;
import entity.Users;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import org.hibernate.Query;
import org.hibernate.Session;
import util.NewHibernateUtil;

/**
 * FXML Controller class
 *
 * @author Administator
 */
public class DaftarUsersController implements Initializable {

    @FXML
    private TableColumn<?, ?> idT;
    @FXML
    private TableColumn<?, ?> namaT;
    @FXML
    private TableColumn<?, ?> tanggalT;
    @FXML
    private TableColumn<?, ?> alamatT;
    @FXML
    private TableColumn<?, ?> notelpT;
    @FXML
    private TableColumn<?, ?> usernameT;
    @FXML
    private TableColumn<?, ?> passwordT;
    @FXML
    private TableColumn<?, ?> fotoT;
    @FXML
    private TextField idF;
    @FXML
    private Button hapusBtn;
    @FXML
    private TableView<Users> tabelUsers;
    ObservableList<Users> data = null;
    @FXML
    private Button menu;
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        data = FXCollections.observableArrayList();
        Model mm = new Model();
        
         for (Users m : mm.findAll()) {
            data.add(m);
            
        idT.setCellValueFactory(new PropertyValueFactory<>("id"));
        namaT.setCellValueFactory(new PropertyValueFactory<>("nama"));
        tanggalT.setCellValueFactory(new PropertyValueFactory<>("tanggalLahir"));
        alamatT.setCellValueFactory(new PropertyValueFactory<>("alamat"));
        notelpT.setCellValueFactory(new PropertyValueFactory<>("notelp"));
        usernameT.setCellValueFactory(new PropertyValueFactory<>("username"));
        passwordT.setCellValueFactory(new PropertyValueFactory<>("password"));
        fotoT.setCellValueFactory(new PropertyValueFactory<>("foto"));
       //tv.getColumns().clear();
        tabelUsers.setItems(data);
    }
    }

    @FXML
    private void hapusAction(ActionEvent event) throws IOException {
        if (idF.getText().equals("")) {
            Alert alert = new Alert(Alert.AlertType.NONE, "Semua field harus di isi", ButtonType.OK);
            alert.setTitle("Gagal");
            alert.showAndWait();
        } else {
             
            executeHQLQueryDelete();
            Alert alert = new Alert(Alert.AlertType.NONE, "Layanan telah dihapus", ButtonType.OK);
            alert.setTitle("Berhasil");
             alert.showAndWait();
        }
        Stage stage;
                Parent root;
                //get reference to the button's stage         
                stage = (Stage) hapusBtn.getScene().getWindow();
                //load up OTHER FXML document
                root = FXMLLoader.load(getClass().getResource("DaftarUsers.fxml"));
                
                Scene scene = new Scene(root);
                stage.setScene(scene);
                stage.show();
    }

    @FXML
    private void fromTabelToField() {
//        tabelUsers.setOnMouseClicked((MouseEvent event)-> {
//            Users p = tabelUsers.getItems().get(tabelUsers.getSelectionModel().getSelectedIndex());
//            
//            idF.setText(p.getId());
//            
       // });
       
        tabelUsers.setOnMouseClicked((MouseEvent event) -> {
            Users kursi = tabelUsers.getItems().get(tabelUsers.getSelectionModel().getSelectedIndex());
            idF.setText(kursi.getNama());
            
        });
    }

    private void executeHQLQueryDelete() {
        try {
            Session session = NewHibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            Query q = session.createSQLQuery ("DELETE FROM users WHERE id = :id");
         
            q.setParameter("id", idF.getText());
            q.executeUpdate();
            session.getTransaction().commit();
            session.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @FXML
    private void menuAction(ActionEvent event) throws IOException {
        Stage stage;
                Parent root;
                //get reference to the button's stage         
                stage = (Stage) hapusBtn.getScene().getWindow();
                //load up OTHER FXML document
                root = FXMLLoader.load(getClass().getResource("/petaniapps/HomepageAdmin.fxml"));
                
                Scene scene = new Scene(root);
                stage.setScene(scene);
                stage.show();
    }
    
}

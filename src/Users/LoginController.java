/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Users;

import entity.Users;
import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import org.hibernate.Query;
import org.hibernate.Session;
import util.NewHibernateUtil;

/**
 * FXML Controller class
 *
 * @author Administator
 */
public class LoginController implements Initializable {

    @FXML
    private TextField usernameF;
    @FXML
    private PasswordField passwordF;
    
    public String savedUsername;
    public String savedPassword;
    @FXML
    private AnchorPane pane;
    @FXML
    private Button loginBtn;
    @FXML
    private Button registerBtn;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    
    private static String QUERY_LOGIN = "from Users a where a.username like '";
      private void runLogin() {
        executeHQLQuery(QUERY_LOGIN + usernameF.getText() + "'");
    }

    @FXML
    private void loginAction(ActionEvent event) {
        runLogin();
    }

    private void executeHQLQuery(String sql) {
         try {
            Session session = NewHibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            Query q = session.createQuery(sql);
            List resultList = q.list();
            for (Object o : resultList) {
                Users user = (Users) o;
                savedUsername = user.getUsername();
                savedPassword = user.getPassword();
            }
            session.getTransaction().commit();
            
            if (savedPassword == null || savedUsername == null) {
                autentikasi(" ", " ");
            }
            else {
                autentikasi(savedPassword, savedUsername);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    private void autentikasi(String savedPassword, String savedUsername) throws IOException {
        if ("root".equals(usernameF.getText()) && "root".equals(passwordF.getText())){
             Stage stage;
                Parent root;
                //get reference to the button's stage         
                stage = (Stage) loginBtn.getScene().getWindow();
                //load up OTHER FXML document
                root = FXMLLoader.load(getClass().getResource("/petaniapps/HomepageAdmin.fxml"));
                
                Scene scene = new Scene(root);
                stage.setScene(scene);
                stage.show();
        }
        else if (savedPassword.equals(passwordF.getText()) && savedUsername.equals(usernameF.getText())) {
            Stage stage;
                Parent root;
                //get reference to the button's stage         
                stage = (Stage) loginBtn.getScene().getWindow();
                //load up OTHER FXML document
                root = FXMLLoader.load(getClass().getResource("/petaniapps/Homepage.fxml"));
                
                Scene scene = new Scene(root);
                stage.setScene(scene);
                stage.show();
        } else {
            Alert alert = new Alert(Alert.AlertType.NONE, "Username atau password salah", ButtonType.OK);
            alert.setTitle("Username atau password salah");
            alert.showAndWait();
        }

    }

    @FXML
    private void registerAction(ActionEvent event) throws IOException {
         Stage stage;
                Parent root;
                //get reference to the button's stage         
                stage = (Stage) loginBtn.getScene().getWindow();
                //load up OTHER FXML document
                root = FXMLLoader.load(getClass().getResource("/Users/Register.fxml"));
                
                Scene scene = new Scene(root);
                stage.setScene(scene);
                stage.show();
    }

   
    
}

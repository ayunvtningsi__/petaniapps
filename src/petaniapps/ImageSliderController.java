/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package petaniapps;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.Cursor;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Administator
 */
public class ImageSliderController implements Initializable {

    @FXML
    private ImageView imageView;
    @FXML
    private Button lbutton;
    @FXML
    private Button rbutton;
     private List<String> list = new ArrayList<String>();
    int j = 0;
    double orgCliskSceneX, orgReleaseSceneX;
    @FXML
    private Button masuk;
  


    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        Stage primaryStage = new Stage();
         try {
        list.add("sawah.jpg");
        list.add("sawahbg.jpg");
        list.add("a.jpg");
        list.add("b.jpg");
        list.add("c.jpg");
        list.add("d.jpg");

        GridPane root = new GridPane();
        root.setAlignment(Pos.CENTER);

        lbutton = new Button("<");
        rbutton = new Button(">");

        Image images[] = new Image[list.size()];
        for (int i = 0; i < list.size(); i++) {
            images[i] = new Image(list.get(i));
        }

        imageView = new ImageView(images[j]);
        imageView.setCursor(Cursor.CLOSED_HAND);

       // imageView.setOnMousePressed(circleOnMousePressedEventHandler);

        imageView.setOnMouseReleased(e -> {
            orgReleaseSceneX = e.getSceneX();
            if (orgCliskSceneX > orgReleaseSceneX) {
                lbutton.fire();
            } else {
                rbutton.fire();
            }
        });

        rbutton.setOnAction(e -> {
            j = j + 1;
            if (j == list.size()) {
                j = 0;
            }
            imageView.setImage(images[j]);

        });
        lbutton.setOnAction(e -> {
            j = j - 1;
            if (j == 0 || j > list.size() + 1 || j == -1) {
                j = list.size() - 1;
            }
            imageView.setImage(images[j]);

        });

        imageView.setFitHeight(500);
        imageView.setFitWidth(600);

        HBox hBox = new HBox();
        hBox.setSpacing(15);
        hBox.setAlignment(Pos.CENTER);
         hBox.getChildren().addAll(lbutton, imageView, rbutton);
//        hBox.getChildren().addAll(imageView);

        root.add(hBox, 1, 1);
        Scene scene = new Scene(root, 800, 300);
        primaryStage.setScene(scene);
        primaryStage.showAndWait();
    } catch (Exception e) {
        e.printStackTrace();
    }
    }

    @FXML
    private void circleOnMousePressedEventHandler(MouseEvent event) {
         
        orgCliskSceneX = event.getSceneX();
    }

    @FXML
    private void masukAction(ActionEvent event) throws IOException {
        Stage stage;
                Parent root;
                //get reference to the button's stage         
                stage = (Stage) masuk.getScene().getWindow();
                //load up OTHER FXML document
                root = FXMLLoader.load(getClass().getResource("/Users/Login.fxml"));
                
                Scene scene = new Scene(root);
                stage.setScene(scene);
                stage.show();
    }
    }
    


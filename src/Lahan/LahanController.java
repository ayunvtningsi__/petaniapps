/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Lahan;



import entity.Lahan;
import java.io.File;
import java.net.URL;
import java.nio.file.Path;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.FileChooser;




import entity.Lahan;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.nio.file.CopyOption;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.embed.swing.SwingFXUtils;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javax.imageio.ImageIO;
import org.hibernate.Query;
import org.hibernate.Session;
import util.NewHibernateUtil;
import Lahan.Model;
import entity.Panen;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Administator
 */
public class LahanController implements Initializable, implementsDao {

    @FXML
    private AnchorPane pane;
    @FXML
    private TextField pemilikF;
    @FXML
    private TextField alamatF;
    @FXML
    private TextArea deskripsiF;
    @FXML
    private Button fotoF;
    
    @FXML
    private TableView<Lahan> TabelLahan;
    @FXML
    private TableColumn pemilikT;
    @FXML
    private TableColumn alamatT;
    @FXML
    private TableColumn deskripsiT;
    @FXML
    private TableColumn fotoT;
    @FXML
    private Button addButton;
    @FXML
    private Button deleteBtn;
    ObservableList<Lahan> data = null;
    
  
    private FileChooser fileChooser;
    private File file;
    private String gambar;
    private Path copy,files;
    private implementsDao implement;
    @FXML
    private Button updateBtn;
    @FXML
    private Button menu;
    @FXML
    private ImageView lihatGambar;
    
    
    

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
          implement = new LahanController();
        fileChooser = new FileChooser();
        fileChooser.getExtensionFilters().addAll(
        new FileChooser.ExtensionFilter("All Files", "*.png","*.jpg","*jpeg","*.bmp", "*.mp4")
    ); 
        data = FXCollections.observableArrayList();
        Model mm = new Model();
        
         for (Lahan m : mm.findAll()) {
            data.add(m);
        }
        
        pemilikT.setCellValueFactory(new PropertyValueFactory<>("pemilik"));
        alamatT.setCellValueFactory(new PropertyValueFactory<>("alamat"));
        deskripsiT.setCellValueFactory(new PropertyValueFactory<>("deskripsi"));
        fotoT.setCellValueFactory(new PropertyValueFactory<>("foto"));
       
        TabelLahan.setItems(data);
    }    

    @FXML
    private void uploadAction(ActionEvent event) {
             file =  fileChooser.showOpenDialog(null);
    if(file != null){
        try {
            BufferedImage bufferedImage = ImageIO.read(file);
            Image image = SwingFXUtils.toFXImage(bufferedImage, null);
            lihatGambar.setFitWidth(180);
            lihatGambar.setFitHeight(110);
            lihatGambar.setPreserveRatio(true);
            lihatGambar.setImage(image);
            gambar = file.getName();
            files = Paths.get(file.toURI());
        } catch (IOException ex) {
            Logger.getLogger(LahanController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    }

    @FXML
    private void fromTableToTextField() {
        TabelLahan.setOnMouseClicked((MouseEvent event) -> {
            Lahan b = TabelLahan.getItems().get(TabelLahan.getSelectionModel().getSelectedIndex());
            pemilikF.setText(b.getPemilik());
            alamatF.setText(b.getAlamat());
            deskripsiF.setText(b.getDeskripsi());
            
        });
        
    }

    @FXML
    private void addAction(ActionEvent event) throws IOException {
         if (pemilikF.getText().equals("")) {
            Alert alert = new Alert(Alert.AlertType.NONE, "Semua field harus di isi", ButtonType.OK);
            alert.setTitle("Gagal");
            alert.showAndWait();
        } else {    
                Lahan upload = new Lahan();
                upload.setPemilik(pemilikF.getText());
                upload.setAlamat(alamatF.getText());
                upload.setDeskripsi(deskripsiF.getText());
                upload.setFoto(gambar);
                implement.createUpload(upload);
        
         if (gambar != null) {
        try {
            File dir = new File(System.getProperty("user.dir"));
            copy = Paths.get(dir+"/src/images/"+gambar);
            CopyOption[] options = new CopyOption[]{
                    StandardCopyOption.REPLACE_EXISTING,
                    StandardCopyOption.COPY_ATTRIBUTES
            };
            Files.copy(files, copy, options);
        } catch (IOException ex) {
            Logger.getLogger(LahanController.class.getName()).log(Level.SEVERE, null, ex);
        }
    } 
                
                
    }
         Stage stage;
                Parent root;
                //get reference to the button's stage         
                stage = (Stage) addButton.getScene().getWindow();
                //load up OTHER FXML document
                root = FXMLLoader.load(getClass().getResource("Lahan.fxml"));
                
                Scene scene = new Scene(root);
                stage.setScene(scene);
                stage.show();
    }

    @FXML
    private void updateAction(ActionEvent event) throws IOException {
         if (pemilikF.getText().equals("")) {
            Alert alert = new Alert(Alert.AlertType.NONE, "Semua field harus di isi", ButtonType.OK);
            alert.setTitle("Gagal");
            alert.showAndWait();
        } else {
             
           executeHQLQueryUpdate();
           Stage stage;
                Parent root;
                //get reference to the button's stage         
                stage = (Stage) addButton.getScene().getWindow();
                //load up OTHER FXML document
                root = FXMLLoader.load(getClass().getResource("Lahan.fxml"));
                
                Scene scene = new Scene(root);
                stage.setScene(scene);
                stage.show();
        }
    }

    @FXML
    private void deleteAction(ActionEvent event) throws IOException {
        
        if (pemilikF.getText().equals("")) {
            Alert alert = new Alert(Alert.AlertType.NONE, "Semua field harus di isi", ButtonType.OK);
            alert.setTitle("Gagal");
            alert.showAndWait();
        } else {
             
            executeHQLQueryDelete();
            Alert alert = new Alert(Alert.AlertType.NONE, "Layanan telah dihapus", ButtonType.OK);
            alert.setTitle("Berhasil");
             alert.showAndWait();
             Stage stage;
                Parent root;
                //get reference to the button's stage         
                stage = (Stage) deleteBtn.getScene().getWindow();
                //load up OTHER FXML document
                root = FXMLLoader.load(getClass().getResource("Lahan.fxml"));
                
                
                Scene scene = new Scene(root);
                stage.setScene(scene);
                stage.show();
        }
    }

    private void executeHQLQueryDelete() {
        try {
            Session session = NewHibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            Query q = session.createSQLQuery ("DELETE FROM lahan WHERE pemilik = :pemilik");
         
            q.setParameter("pemilik", pemilikF.getText());
            

            q.executeUpdate();
            
            session.getTransaction().commit();
            
            session.close();
            
            //pindah();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    

    @Override
    public void createUpload(Lahan upload) {
          Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Apakah anda akan menyimpan data");
        alert.setHeaderText(null);
        alert.setContentText("Tekan OK untuk menyimpan data, Cencel untuk batal.");
        Optional result = alert.showAndWait();
        
        if (result.get() == ButtonType.OK){
            Session session = NewHibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
             //Query q = session.createSQLQuery("INSERT INTO upload (foto) VALUES (:foto)");
              Query q = session.createSQLQuery("INSERT INTO lahan (pemilik,alamat,deskripsi,foto) VALUES (?,?,?,?)");
            
            q.setString(0, upload.getPemilik());
            q.setString(1, upload.getAlamat());
            q.setString(2, upload.getDeskripsi());
            q.setString(3, upload.getFoto());
            q.executeUpdate();
            session.getTransaction().commit();
            session.close();
    
}
  }

    private void executeHQLQueryUpdate() {
        try {
            Session session = NewHibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            Query q = session.createSQLQuery ("UPDATE lahan SET  pemilik = :pemilik ,alamat =:alamat, deskripsi =:deskripsi Where pemilik = :pemilik");
         
            q.setParameter("pemilik", pemilikF.getText());
            q.setParameter("alamat", alamatF.getText());
            
            q.setParameter("deskripsi", deskripsiF.getText());
           
           
            q.executeUpdate();
            
            session.getTransaction().commit();
            
            session.close();
            
            //pindah();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @FXML
    private void menuAction(ActionEvent event) throws IOException {
        Stage stage;
                Parent root;
                //get reference to the button's stage         
                stage = (Stage) menu.getScene().getWindow();
                //load up OTHER FXML document
                root = FXMLLoader.load(getClass().getResource("/petaniapps/Homepage.fxml"));
                
                Scene scene = new Scene(root);
                stage.setScene(scene);
                stage.show();
    }
}

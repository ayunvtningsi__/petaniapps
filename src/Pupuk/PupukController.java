/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Pupuk;

import Lahan.LahanController;
import entity.Lahan;
import entity.Pupuk;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.CopyOption;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javax.imageio.ImageIO;
import org.hibernate.Query;
import org.hibernate.Session;
import util.NewHibernateUtil;

/**
 * FXML Controller class
 *
 * @author Administator
 */
public class PupukController implements Initializable, implementsDao {
    @FXML
    private AnchorPane pane;
    @FXML
    private TextField namaF;
    @FXML
    private TextField hargaF;
    @FXML
    private TextArea KegunaanF;
    @FXML
    private Button fotoF;
    @FXML
    private TableView<Pupuk> TabelPupuk;
    @FXML
    private TableColumn<?, ?> NamaT;
    @FXML
    private TableColumn<?, ?> HargaT;
    @FXML
    private TableColumn<?, ?> KegunaaanT;
    @FXML
    private TableColumn<?, ?> fotoT;
    @FXML
    private Button addButton;
    @FXML
    private Button updateButton;
    @FXML
    private Button deleteBtn;
    
    
    
    ObservableList<Pupuk> data = null;
   
    private FileChooser fileChooser;
    private File file;
    private String gambar;
    private Path copy,files;
    private implementsDao implement;
    private Button refreshBtn;
    @FXML
    private Button menu;
    @FXML
    private ImageView lihatGambar;
    

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        implement = new PupukController();
        fileChooser = new FileChooser();
        fileChooser.getExtensionFilters().addAll(
        new FileChooser.ExtensionFilter("All Files", "*.png","*.jpg","*jpeg","*.bmp", "*.mp4")
    ); 
        data = FXCollections.observableArrayList();
        Model mm = new Model();
        
         for (Pupuk m : mm.findAll()) {
            data.add(m);
        }
        
        NamaT.setCellValueFactory(new PropertyValueFactory<>("nama"));
        HargaT.setCellValueFactory(new PropertyValueFactory<>("harga"));
        KegunaaanT.setCellValueFactory(new PropertyValueFactory<>("kegunaan"));
        fotoT.setCellValueFactory(new PropertyValueFactory<>("foto"));
       
        TabelPupuk.setItems(data);
    }    
    @FXML
    private void uploadAction(ActionEvent event) {
        file =  fileChooser.showOpenDialog(null);
        if(file != null){
        try {
            BufferedImage bufferedImage = ImageIO.read(file);
            Image image = SwingFXUtils.toFXImage(bufferedImage, null);
            lihatGambar.setFitWidth(180);
            lihatGambar.setFitHeight(110);
            lihatGambar.setPreserveRatio(true);
            lihatGambar.setImage(image);
            gambar = file.getName();
            files = Paths.get(file.toURI());
        } catch (IOException ex) {
            Logger.getLogger(PupukController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    }

    @FXML
    private void fromTableToTextField() {
        TabelPupuk.setOnMouseClicked((MouseEvent event) -> {
            Pupuk b = TabelPupuk.getItems().get(TabelPupuk.getSelectionModel().getSelectedIndex());
            namaF.setText(b.getNama());
            hargaF.setText(b.getHarga());
            KegunaanF.setText(b.getKegunaan());
            
        });
        
    }

    @FXML
    private void addAction(ActionEvent event) throws IOException {
  if (namaF.getText().equals("")) {
            Alert alert = new Alert(Alert.AlertType.NONE, "Semua field harus di isi", ButtonType.OK);
            alert.setTitle("Gagal");
            alert.showAndWait();
        } else {    
                Pupuk upload = new Pupuk();
                upload.setNama(namaF.getText());
                upload.setHarga(hargaF.getText());
                upload.setKegunaan(KegunaanF.getText());
                upload.setFoto(gambar);
                implement.createUpload(upload);
        
         if (gambar != null) {
        try {
            File dir = new File(System.getProperty("user.dir"));
            copy = Paths.get(dir+"/src/images/"+gambar);
            CopyOption[] options = new CopyOption[]{
                    StandardCopyOption.REPLACE_EXISTING,
                    StandardCopyOption.COPY_ATTRIBUTES
            };
            Files.copy(files, copy, options);
        } catch (IOException ex) {
            Logger.getLogger(PupukController.class.getName()).log(Level.SEVERE, null, ex);
        }
    } 
                
                
    }
  Stage stage;
                Parent root;
                //get reference to the button's stage         
                stage = (Stage) addButton.getScene().getWindow();
                //load up OTHER FXML document
                root = FXMLLoader.load(getClass().getResource("Pupuk.fxml"));
                
                Scene scene = new Scene(root);
                stage.setScene(scene);
                stage.show();
    }

    @FXML
    private void updateAction(ActionEvent event) throws IOException {
      if (namaF.getText().equals("")) {
            Alert alert = new Alert(Alert.AlertType.NONE, "Semua field harus di isi", ButtonType.OK);
            alert.setTitle("Gagal");
            alert.showAndWait();
        } else {
             
           executeHQLQueryUpdate();
           Stage stage;
                Parent root;
                //get reference to the button's stage         
                stage = (Stage) updateButton.getScene().getWindow();
                //load up OTHER FXML document
                root = FXMLLoader.load(getClass().getResource("Pupuk.fxml"));
                
                Scene scene = new Scene(root);
                stage.setScene(scene);
                stage.show();

        }
    }

    @FXML
    private void deleteAction(ActionEvent event) throws IOException {
    
        if (namaF.getText().equals("")) {
            Alert alert = new Alert(Alert.AlertType.NONE, "Semua field harus di isi", ButtonType.OK);
            alert.setTitle("Gagal");
            alert.showAndWait();
        } else {
             
            executeHQLQueryDelete();
            Alert alert = new Alert(Alert.AlertType.NONE, "Layanan telah dihapus", ButtonType.OK);
            alert.setTitle("Berhasil");
             alert.showAndWait();
             
             Stage stage;
                Parent root;
                //get reference to the button's stage         
                stage = (Stage) deleteBtn.getScene().getWindow();
                //load up OTHER FXML document
                root = FXMLLoader.load(getClass().getResource("Pupuk.fxml"));
                
                Scene scene = new Scene(root);
                stage.setScene(scene);
                stage.show();
        }
    }

    private void executeHQLQueryDelete() {
       try {
            Session session = NewHibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            Query q = session.createSQLQuery ("DELETE FROM pupuk WHERE nama = :nama");
         
            q.setParameter("nama", namaF.getText());
            

            q.executeUpdate();
            
            session.getTransaction().commit();
            
            session.close();
            
            //pindah();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void executeHQLQueryUpdate() {
        try {
            Session session = NewHibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            Query q = session.createSQLQuery ("UPDATE pupuk SET  nama = :nama ,harga =:harga, kegunaan =:kegunaan Where nama = :nama");
         
            q.setParameter("nama", namaF.getText());
            q.setParameter("harga", hargaF.getText());
            
            q.setParameter("kegunaan", KegunaanF.getText());
            q.executeUpdate();
            session.getTransaction().commit();
            session.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void createUpload(Pupuk upload) {
           Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Apakah anda akan menyimpan data");
        alert.setHeaderText(null);
        alert.setContentText("Tekan OK untuk menyimpan data, Cencel untuk batal.");
        Optional result = alert.showAndWait();
        
        if (result.get() == ButtonType.OK){
            Session session = NewHibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
             //Query q = session.createSQLQuery("INSERT INTO upload (foto) VALUES (:foto)");
              Query q = session.createSQLQuery("INSERT INTO pupuk (nama,harga,kegunaan,foto) VALUES (?,?,?,?)");
            
            q.setString(0, upload.getNama());
            q.setString(1, upload.getHarga());
            q.setString(2, upload.getKegunaan());
            q.setString(3, upload.getFoto());
            q.executeUpdate();
            session.getTransaction().commit();
            session.close();
    
}
    }

    @FXML
    private void menuAction(ActionEvent event) throws IOException {
        Stage stage;
                Parent root;
                //get reference to the button's stage         
                stage = (Stage) addButton.getScene().getWindow();
                //load up OTHER FXML document
                root = FXMLLoader.load(getClass().getResource("/petaniapps/Homepage.fxml"));
                
                Scene scene = new Scene(root);
                stage.setScene(scene);
                stage.show();
    }
}

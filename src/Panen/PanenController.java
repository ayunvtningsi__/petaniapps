/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Panen;

import Lahan.LahanController;
import entity.Lahan;
import entity.Panen;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.CopyOption;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javax.imageio.ImageIO;
import org.hibernate.Query;
import org.hibernate.Session;
import util.NewHibernateUtil;

/**
 * FXML Controller class
 *
 * @author Administator
 */
public class PanenController implements Initializable, implementsDao {

    @FXML
    private AnchorPane pane;
    @FXML
    private TextField namaPetaniF;
    @FXML
    private TextField alamatLahanF;
    @FXML
    private TextArea StatusF;
    @FXML
    private Button fotoF;
    @FXML
    private TableView<Panen> TabelPanen;
    @FXML
    private TableColumn<?, ?> NamaT;
    @FXML
    private TableColumn<?, ?> AlamatT;
    @FXML
    private TableColumn<?, ?> PenanamanT;
    @FXML
    private TableColumn<?, ?> PrapanenT;
    @FXML
    private TableColumn<?, ?> PanenT;
    @FXML
    private TableColumn<?, ?> paskahPanenT;
    @FXML
    private TableColumn<?, ?> statusT;
    @FXML
    private TableColumn<?, ?> FotoT;
    @FXML
    private Button addButton;
    @FXML
    private Button updateButton;
    @FXML
    private Button deleteBtn;
    @FXML
    private TextField PrapanenF;
    @FXML
    private TextField penanamanF;
    @FXML
    private TextField panenF;
    @FXML
    private TextField paskahPanenF;
    
    
    ObservableList<Panen> data = null;
    
  
    private FileChooser fileChooser;
    private File file;
    private String gambar;
    private Path copy,files;
    private implementsDao implement;
    @FXML
    private Button menu;
    @FXML
    private ImageView lihatGambar;
    

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
       implement = new PanenController();
        fileChooser = new FileChooser();
        fileChooser.getExtensionFilters().addAll(
        new FileChooser.ExtensionFilter("All Files", "*.png","*.jpg","*jpeg","*.bmp", "*.mp4")
    ); 
        data = FXCollections.observableArrayList();
        Model mm = new Model();
        
         for (Panen m : mm.findAll()) {
            data.add(m);
        }
        
        NamaT.setCellValueFactory(new PropertyValueFactory<>("namaPetani"));
        AlamatT.setCellValueFactory(new PropertyValueFactory<>("alamatLahan"));
        PenanamanT.setCellValueFactory(new PropertyValueFactory<>("penanaman"));
        PrapanenT.setCellValueFactory(new PropertyValueFactory<>("prapanen"));
        PanenT.setCellValueFactory(new PropertyValueFactory<>("panen"));
        paskahPanenT.setCellValueFactory(new PropertyValueFactory<>("paskapanen"));
        statusT.setCellValueFactory(new PropertyValueFactory<>("status"));
        FotoT.setCellValueFactory(new PropertyValueFactory<>("foto"));
        TabelPanen.setItems(data);
    }    

    @FXML
    private void uploadAction(ActionEvent event) { 
        file =  fileChooser.showOpenDialog(null);
    if(file != null){
        try {
            BufferedImage bufferedImage = ImageIO.read(file);
            Image image = SwingFXUtils.toFXImage(bufferedImage, null);
            lihatGambar.setFitWidth(180);
            lihatGambar.setFitHeight(110);
            lihatGambar.setPreserveRatio(true);
            lihatGambar.setImage(image);
            gambar = file.getName();
            files = Paths.get(file.toURI());
        } catch (IOException ex) {
            Logger.getLogger(LahanController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    }

    @FXML
    private void fromTableToTextField() {
        TabelPanen.setOnMouseClicked((MouseEvent event) -> {
            Panen b = TabelPanen.getItems().get(TabelPanen.getSelectionModel().getSelectedIndex());
            namaPetaniF.setText(b.getNamaPetani());
            alamatLahanF.setText(b.getAlamatLahan());
            StatusF.setText(b.getStatus());
            fotoF.setText(b.getFoto());
            PrapanenF.setText(b.getPrapanen());
            penanamanF.setText(b.getPenanaman());
            panenF.setText(b.getPanen());
            paskahPanenF.setText(b.getPaskapanen());
            
        });
        
    }

    @FXML
    private void addAction(ActionEvent event) throws IOException {
        if (namaPetaniF.getText().equals("")) {
            Alert alert = new Alert(Alert.AlertType.NONE, "Semua field harus di isi", ButtonType.OK);
            alert.setTitle("Gagal");
            alert.showAndWait();
        } else {    
                Panen upload = new Panen();
                upload.setNamaPetani(namaPetaniF.getText());
                upload.setAlamatLahan(alamatLahanF.getText());
                upload.setPenanaman(penanamanF.getText());
                upload.setPrapanen(PrapanenF.getText());
                upload.setPanen(panenF.getText());
                upload.setPaskapanen(paskahPanenF.getText());
                upload.setStatus(StatusF.getText());
                upload.setFoto(gambar);
                implement.createUpload(upload);
        
         if (gambar != null) {
        try {
            File dir = new File(System.getProperty("user.dir"));
            copy = Paths.get(dir+"/src/images/"+gambar);
            CopyOption[] options = new CopyOption[]{
                    StandardCopyOption.REPLACE_EXISTING,
                    StandardCopyOption.COPY_ATTRIBUTES
            };
            Files.copy(files, copy, options);
        } catch (IOException ex) {
            Logger.getLogger(PanenController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }          
    }
        Stage stage;
                Parent root;
                //get reference to the button's stage         
                stage = (Stage) addButton.getScene().getWindow();
                //load up OTHER FXML document
                root = FXMLLoader.load(getClass().getResource("Panen.fxml"));
                
                Scene scene = new Scene(root);
                stage.setScene(scene);
                stage.show();
    }

    @FXML
    private void updateAction(ActionEvent event) throws IOException {
         if (namaPetaniF.getText().equals("")) {
            Alert alert = new Alert(Alert.AlertType.NONE, "Semua field harus di isi", ButtonType.OK);
            alert.setTitle("Gagal");
            alert.showAndWait();
        } else {
           executeHQLQueryUpdate();
           Stage stage;
                Parent root;
                //get reference to the button's stage         
                stage = (Stage) addButton.getScene().getWindow();
                //load up OTHER FXML document
                root = FXMLLoader.load(getClass().getResource("Panen.fxml"));
                
                Scene scene = new Scene(root);
                stage.setScene(scene);
                stage.show();

        }
    }

    @FXML
    private void deleteAction(ActionEvent event) throws IOException {
        
        if (namaPetaniF.getText().equals("")) {
            Alert alert = new Alert(Alert.AlertType.NONE, "Semua field harus di isi", ButtonType.OK);
            alert.setTitle("Gagal");
            alert.showAndWait();
        } else {
             
            executeHQLQueryDelete();
            Alert alert = new Alert(Alert.AlertType.NONE, "Layanan telah dihapus", ButtonType.OK);
            alert.setTitle("Berhasil");
             alert.showAndWait();
        }
        
        Stage stage;
                Parent root;
                //get reference to the button's stage         
                stage = (Stage) addButton.getScene().getWindow();
                //load up OTHER FXML document
                root = FXMLLoader.load(getClass().getResource("Panen.fxml"));
                
                Scene scene = new Scene(root);
                stage.setScene(scene);
                stage.show();
    }

    private void executeHQLQueryDelete() {
        try {
            Session session = NewHibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            Query q = session.createSQLQuery ("DELETE FROM panen WHERE nama_petani = :nama_petani");
         
            q.setParameter("nama_petani", namaPetaniF.getText());
            

            q.executeUpdate();
            
            session.getTransaction().commit();
            
            session.close();
            
            //pindah();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void createUpload(Panen upload) {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Apakah anda akan menyimpan data");
        alert.setHeaderText(null);
        alert.setContentText("Tekan OK untuk menyimpan data, Cencel untuk batal.");
        Optional result = alert.showAndWait();
        
        if (result.get() == ButtonType.OK){
            Session session = NewHibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
             //Query q = session.createSQLQuery("INSERT INTO upload (foto) VALUES (:foto)");
              Query q = session.createSQLQuery("INSERT INTO panen (nama_petani,alamat_lahan,penanaman,prapanen,panen, paskapanen, status,foto) VALUES (?,?,?,?,?,?,?,?)");
            
            q.setString(0, upload.getNamaPetani());
            q.setString(1, upload.getAlamatLahan());
            q.setString(2, upload.getPenanaman());
            q.setString(3, upload.getPrapanen());
            q.setString(4, upload.getPanen());
            q.setString(5, upload.getPaskapanen());
            q.setString(6, upload.getStatus());
            q.setString(7, upload.getFoto());
            q.executeUpdate();
            session.getTransaction().commit();
            session.close();
    }
}

    private void executeHQLQueryUpdate() {
        try {
            Session session = NewHibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            Query q = session.createSQLQuery ("UPDATE panen SET  nama_petani =:nama_petani, alamat_lahan =:alamat_lahan, penanaman =:penanaman, prapanen =:prapanen, panen =:panen, paskapanen =:paskapanen, status =:status Where nama_petani = :nama_petani");
         
            q.setParameter("nama_petani", namaPetaniF.getText());
            q.setParameter("alamat_lahan", alamatLahanF.getText());
            q.setParameter("penanaman", penanamanF.getText());
            q.setParameter("prapanen", PrapanenF.getText());
            q.setParameter("panen", panenF.getText());
            q.setParameter("paskapanen", paskahPanenF.getText());
            q.setParameter("status", StatusF.getText());

           
           
            q.executeUpdate();
            
            session.getTransaction().commit();
            
            session.close();
            
            //pindah();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @FXML
    private void menuAction(ActionEvent event) throws IOException {
        Stage stage;
                Parent root;
                //get reference to the button's stage         
                stage = (Stage) addButton.getScene().getWindow();
                //load up OTHER FXML document
                root = FXMLLoader.load(getClass().getResource("/petaniapps/Homepage.fxml"));
                
                Scene scene = new Scene(root);
                stage.setScene(scene);
                stage.show();
    }
    
}
